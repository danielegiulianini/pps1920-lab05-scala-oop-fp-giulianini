package u04lab.code

import scala.annotation.tailrec
import scala.language.postfixOps // silence warnings

sealed trait List[A] {

  def head: Option[A]

  def tail: Option[List[A]]

  def append(list: List[A]): List[A]

  def foreach(consumer: (A) => Unit): Unit

  def get(pos: Int): Option[A]

  def filter(predicate: (A) => Boolean): List[A]

  def map[B](fun: (A) => B): List[B]

  def toSeq: Seq[A]

  def foldLeft[B](acc: B)(f: (B,A)=>B): B

  def foldRight[B](acc: B)(f: (A,B)=>B): B

  def flatMap[B](f: A => List[B]): List[B]

  def reverse(): List[A]

  def zipRight: List[(A,Int)]

  def partition(pred: A => Boolean): (List[A],List[A])

  def span(pred: A => Boolean): (List[A],List[A])

  def reduce(op: (A,A)=>A): A

  def takeRight(n: Int): List[A]

  def collect[B](partialFunction: PartialFunction[A, B]) : List[B]


  // right-associative construction: 10 :: 20 :: 30 :: Nil()
  def ::(head: A): List[A] = Cons(head,this)


}

// defining concrete implementations based on the same template

case class Cons[A](_head: A, _tail: List[A])
  extends ListImplementation[A]

case class Nil[A]()
  extends ListImplementation[A]

// enabling pattern matching on ::

object :: {
  def unapply[A](l: List[A]): Option[(A,List[A])] = l match {
    case Cons(h,t) => Some((h,t))
    case _ => None
  }
}

// List algorithms
trait ListImplementation[A] extends List[A] {

  override def head: Option[A] = this match {
    case h :: t => Some(h)
    case _ => None
  }
  override def tail: Option[List[A]] = this match {
    case h :: t => Some(t)
    case _ => None
  }
  override def append(list: List[A]): List[A] = this match {
    case h :: t => h :: (t append list)
    case _ => list
  }
  override def foreach(consumer: (A)=>Unit): Unit = this match {
    case h :: t => {consumer(h); t foreach consumer}
    case _ => None
  }
  override def get(pos: Int): Option[A] = this match {
    case h :: t if pos == 0 => Some(h)
    case h :: t if pos > 0 => t get (pos-1)
    case _ => None
  }
  override def filter(predicate: (A) => Boolean): List[A] = this match {
    case h :: t if (predicate(h)) => h :: (t filter predicate)
    case _ :: t => (t filter predicate)
    case _ => Nil()
  }

  override def map[B](fun: (A) => B): List[B] = this match {
    case h :: t => fun(h) :: (t map fun)
    case _ => Nil()
  }

  override def toSeq: Seq[A] = this match {
    case h :: t => h +: t.toSeq // using method '+:' in Seq..
    case _ => Seq()
  }

  override def foldLeft[B](acc: B)(f: (B,A)=>B): B = this match {
    case Cons(h,t) => t.foldLeft(f(acc,h))(f)
    case Nil() => acc
  }

  override def foldRight[B](acc: B)(f: (A, B) => B): B =
    this.reverse().foldLeft(acc)((acc,elem) => f(elem,acc))

  override def reverse(): List[A] =
    this.foldLeft(Nil[A].asInstanceOf[List[A]])((acc,elem) => Cons(elem,acc))

  override def flatMap[B](f: A => List[B]): List[B] = this match {
    case Cons(h,t) => f(h).append(t.flatMap(f))
    case Nil() => Nil()
  }



  //ex. 1.1
  override def zipRight: List[(A,Int)] = {
    def zipRightWithIndex(l: List[A], n:Int) : List[(A, Int)] = l match {
      case h :: t => Cons((h, n), zipRightWithIndex(t, n+1))
      case _ => Nil()
    }
    zipRightWithIndex(this, 0)
  }

  def zipRight2:  List[(A,Int)] = {
    var index = 0
    var list : List[(A, Int)] = Nil()
    this foreach(x => {
      list = Cons((x, index), list);
      index = index+1
    })
    list
  }

  //ex. 1.2
  override def partition(pred: A => Boolean): (List[A],List[A]) = this match {
    case h :: t => val rec = t.partition(pred);
      if (pred(h)) (Cons(h, rec._1), rec._2) else (rec._1, Cons(h, rec._2))
    case _ => (Nil(), Nil())
  }

  //ex. 1.3
  override def span(pred: A => Boolean): (List[A],List[A]) = this match {
    case h :: t if pred(h) => val rec = t.span(pred); (Cons(h, rec._1), rec._2)
    case _ => (Nil(),this)
  }


  //ex 1.4
  /**
    *
    * @throws UnsupportedOperationException if the list is empty
    */
  override def reduce(op: (A,A)=>A): A = this match {
    case h::t if t == Nil() => h
    case h::t => op(h, t.reduce(op))
    case _ => throw new UnsupportedOperationException
  }

  def reduceByFoldLeft(op: (A,A)=>A): A = this match {
    case h::t if t == Nil() => h
    case h::t => t.foldLeft(h)(op)  //foldLeft is tail recursive
    case _ => throw new UnsupportedOperationException
  }

  //ex 1.5
  override def takeRight(toTake:Int) : List[A] = {
    def takeNRight(l: List[A], toTake: Int): (List[A], Int) = l match {   //def that also keeps track of tail length
      case h :: t => val rec = takeNRight(t, toTake);
        if (rec._2 < toTake) (Cons(h, t), rec._2 + 1) else (rec._1, toTake) //._2 is length of tail, if ._2==toTake: finished
      case _ => (Nil(), 0)
    }
    takeNRight(this, toTake)._1
  }

  //methods added only here to not clutter trait
  def drop(l:List[A])(n:Int) : List[A] = l match {
    case h::t if n <= 0 || l == Nil() => l
    case h::t => drop(t)(n-1)
  }
  def size(l: List[A]) : Int = l match {
    case Nil() => 0
    case h:: t => 1 + size(t)
  }
  def takeRight2(n: Int): List[A] = drop(this)(size(this)-n)


  //ex 1.6
  override def collect[B](pf: PartialFunction[A, B]) : List[B] = this match {
    case h::t if pf isDefinedAt h => pf(h) ::  t.collect(pf)
    case h::t => t.collect(pf)
    case _ => Nil()
  }

  def collect2[B](pf: PartialFunction[A, B]) : List[B] = flatMap(x => if (pf isDefinedAt x ) List(pf(x)) else Nil())

}

// Factories
object List {

  // Smart constructors
  def nil[A]: List[A] = Nil()
  def cons[A](h: A, t: List[A]): List[A] = Cons(h,t)

  def apply[A](elems: A*): List[A] = {
    var list: List[A] = Nil()
    for (i <- elems.length-1 to 0 by -1) list = elems(i) :: list
    list
  }

  def of[A](elem: A, n: Int): List[A] =
    if (n==0) Nil() else elem :: of(elem,n-1)
}