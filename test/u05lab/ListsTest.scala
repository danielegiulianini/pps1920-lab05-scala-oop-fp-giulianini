package u05lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u04lab.code.{Cons, List, Nil}

class ListsTest {

  import List._  // Working with the above lists
  val l = 10 :: 20 :: 30 :: 40 :: Nil() // same as above

  @Test
  def testHead() : Unit = assertEquals((l.head), Some(10))
  @Test
  def testTail() : Unit = assertEquals(l.tail, Some(List(20,30,40)))
  @Test
  def testAppend() : Unit = assertEquals(l append l, List(10,20,30,40,10,20,30,40))
  @Test
  def testToSeq : Unit = assertEquals(l append l toSeq, scala.Seq(10,20,30,40,10,20,30,40)) // as a list: 10,20,30,40,10,20,30,40
  @Test
  def testGet() : Unit = assertEquals(l get 2, Some(30))
  @Test
  def testOf() : Unit = assertEquals(of("a", 10), List("a","a", "a","a","a","a","a","a","a","a"))
  @Test
  def testFilterAndMap : Unit = assertEquals(l filter (_<=20) map ("a"+_) , List("a10", "a20"))

  //assert(List(1,2,3) == List(1,2,3))

  //println(scala.collection.immutable.List(10,20,30,40).partition(_>15))
  //println(scala.collection.immutable.List(10,20,30,40).span(_>15))

  //ex 1.1
  @Test
  def testZipRight() : Unit = assertEquals(l.zipRight.toSeq, scala.Seq((10,0), (20,1), (30,2), (40,3)))

  // Ex. 1.2: partition
  @Test
  def testPartition() : Unit = assertEquals(l.partition(_>15), (Cons(20,Cons(30,Cons(40,Nil()))), Cons(10,Nil())))

  // Ex. 1.3: span
  @Test
  def testSpan() : Unit = {
    assertEquals(l.span(_>15), ( Nil(), Cons(10,Cons(20,Cons(30,Cons(40,Nil())))) ))
    assertEquals(l.span(_<15), ( Cons(10,Nil()), Cons(20,Cons(30,Cons(40,Nil()))) ))
  }

  // Ex. 1.4: reduce
  @Test
  def testReduce() : Unit = {
    assertEquals(l.reduce(_+_), 100)
    assertThrows(classOf[UnsupportedOperationException], () => List[Int]().reduce(_+_))
  }

  // Ex. 1.5: takeRight
  @Test
  def testTakeRight() = assertEquals(l.takeRight(2), Cons(30,Cons(40,Nil())))

  // Ex. 1.6: collect
  @Test
  def testCollect = assertEquals(l.collect { case x if x<15 || x>35 => x-1 },  Cons(9, Cons(39, Nil())))

}